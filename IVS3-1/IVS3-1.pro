#-------------------------------------------------
#
# Project created by QtCreator 2014-04-13T21:00:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = IVS3-1
TEMPLATE = app


SOURCES += main.cpp\
        calc.cpp

HEADERS  += calc.h

FORMS    += calc.ui
