/**
 * @file main.cpp
 * @date 17.4.2014
 * @author Tomáš Kello
 * @brief Balík main - obsahuje volania funkcií na obsluhu kalkulačky
 */
             
#include "calc.h"
#include <QApplication>

/**
 * Funkce main
 * @brief Spustí program kalkulačka
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Calc w;
    w.show();
    
    return a.exec();
}

/*** Konec souboru main.cpp ***/
