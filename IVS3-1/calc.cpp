/**
 * @file calc.cpp
 * @date 17.4.2014
 * @author Tomáš Kello
 * @brief Balík calc - obsahuje implementované metódy na obsluhu kalkulačky
 */
   
#include "calc.h"
#include "ui_calc.h"
int comma = 0;          /**< True když byla zadána desetinná tečka. */

/**
 * @brief Zavolá se při zmáčknutí klávesy z klávesnice. 
 * @brief Definuje Která klávesa z klávesnice zavolá jakou funkci. Funguje stejně jako zmáčknutí tlačítka.
 * 
 * @param event Struktura obsahující zmášknutou klávesu
 */ 
void Calc::keyPressEvent(QKeyEvent *event)
{
    /**
     * Tlačítka s čísly
     */         
    if(event->key() == Qt::Key_0)
        Calc::on_N0_clicked();
    if(event->key() == Qt::Key_1)
        Calc::on_N1_clicked();
    if(event->key() == Qt::Key_2)
        Calc::on_N2_clicked();
    if(event->key() == Qt::Key_3)
        Calc::on_N3_clicked();
    if(event->key() == Qt::Key_4)
        Calc::on_N4_clicked();
    if(event->key() == Qt::Key_5)
        Calc::on_N5_clicked();
    if(event->key() == Qt::Key_6)
        Calc::on_N6_clicked();
    if(event->key() == Qt::Key_7)
        Calc::on_N7_clicked();
    if(event->key() == Qt::Key_8)
        Calc::on_N8_clicked();
    if(event->key() == Qt::Key_9)
        Calc::on_N9_clicked();    
    /**
     * Funkční tlačítka.
     */             
    if(event->key() == Qt::Key_Equal)
        Calc::on_vypocet_clicked();
    if(event->key() == Qt::Key_Enter)
        Calc::on_vypocet_clicked();
    if(event->key() == Qt::Key_Comma)
        Calc::on_comma_clicked();
    if(event->key() == Qt::Key_Period)
        Calc::on_comma_clicked();
    if(event->key() == Qt::Key_Exclam)
        Calc::on_Fact_clicked();
    if(event->key() == Qt::Key_Plus)
        Calc::on_plus_clicked();
    if(event->key() == Qt::Key_Minus)
        Calc::on_minus_clicked();
    if(event->key() == Qt::Key_multiply)
        Calc::on_krat_clicked();        //TODO zistit cislo hviezdicky
    if(event->key() == Qt::Key_Slash)
        Calc::on_deleno_clicked();
    if(event->key() == Qt::Key_Delete)
        Calc::on_clear_clicked();
}

/**
 * @brief Vytvoří okýnko kalkulačky.
 */  
Calc::Calc(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Calc)
{
    ui->setupUi(this);
    ui->Operand1->setText(QString::number(0));
    ui->Operand2->setText(QString::number(0));
}

Calc::~Calc()
{
    delete ui;
}

/**
 * @brief Zavolá se při zmáčknutí tlačítka "=", a provede výpočet.
 */   
void Calc::on_vypocet_clicked()
{
    comma = false;
    if(ui->znak->text() == "+")
    {
        ui->Operand2->setText(QString::number(ui->Operand2->text().toDouble()+ui->Operand1->text().toDouble()));
        ui->Operand1->setText(QString::number(0));
    }
    else
        if(ui->znak->text() == "-")
        {
            ui->Operand2->setText(QString::number(ui->Operand2->text().toDouble()-ui->Operand1->text().toDouble()));
            ui->Operand1->setText(QString::number(0));
        }
        else
            if(ui->znak->text() == "*")
            {
                ui->Operand2->setText(QString::number(ui->Operand2->text().toDouble()*ui->Operand1->text().toDouble()));
                ui->Operand1->setText(QString::number(0));
            }
            else
                    if(ui->znak->text() == "/")
                    {
                        ui->Operand2->setText(QString::number(ui->Operand2->text().toDouble()/ui->Operand1->text().toDouble()));
                        ui->Operand1->setText(QString::number(0));
                    }
}

/**
 * @brief Zavolá se při zmáčknutí tlačítka "+".
 * @brief Sčíta dve čísla.
 */
void Calc::on_plus_clicked()
{
    ui->znak->setText((QString)"+");
    cislo;
}

/**
 * 
 * @brief Zavolá se při zmáčknutí tlačítka "-".
 * @brief Odčíta dve čísla.
 */
void Calc::on_minus_clicked()
{
    ui->znak->setText((QString)"-");
    cislo;
}

/**
 * @brief Zavolá se při zmáčknutí tlačítka "×".
 * @brief Vynásobí dve čísla.
 */
void Calc::on_krat_clicked()
{
    ui->znak->setText((QString)"*");
    cislo;
}

/**
 * Metoda on_deleno_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "÷".
 * @brief Vydelí dve čísla.
 */
void Calc::on_deleno_clicked()
{
    ui->znak->setText((QString)"/");
    cislo;
}

/**
 * Metoda on_clear_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "CLR".
 * @brief Vymaže hodnoty z pamäte.
 */
void Calc::on_clear_clicked()
{
    ui->Operand1->setText(QString::number(0));
    ui->Operand2->setText(QString::number(0));
    comma = false;
}

/**
 * Metoda on_N1_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "1".
 */
void Calc::on_N1_clicked()
{
    pripocitaj("1")
}

/**
 * Metoda on_N2_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "2".
 */
void Calc::on_N2_clicked()
{
    pripocitaj("2");
}

/**
 * Metoda on_N3_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "3".
 */
void Calc::on_N3_clicked()
{
    pripocitaj("3");
}

/**
 * Metoda on_N4_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "4".
 */
void Calc::on_N4_clicked()
{
    pripocitaj("4");
}

/**
 * Metoda on_N5_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "5".
 */
void Calc::on_N5_clicked()
{
    pripocitaj("5");
}

/**
 * Metoda on_N6_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "6".
 */
void Calc::on_N6_clicked()
{
    pripocitaj("6");
}

/**
 * Metoda on_N7_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "7".
 */
void Calc::on_N7_clicked()
{
    pripocitaj("7");
}

/**
 * Metoda on_N8_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "8".
 */
void Calc::on_N8_clicked()
{
    pripocitaj("8");
}

/**
 * Metoda on_N9_clicked
 * @brief Zavolá se při zmáčknutí tlačítka "9".
 */
void Calc::on_N9_clicked()
{
    pripocitaj("9");
}

/**
 * Metoda on_N0_clicked
 * 
 * @brief Zavolá se při zmáčknutí tlačítka "0".
 */
void Calc::on_N0_clicked()
{
    pripocitaj("0");
}

/**
 * Metoda on_comma_clicked
 * 
 * @brief Zavolá se při zmáčknutí tlačítka ",".
 */
void Calc::on_comma_clicked()
{
    if(!comma)
    {
        pripocitaj(".");
        comma = true;
    }
}

/**
 * Metoda on_Fact_clicked
 * 
 * @brief Zavolá se při zmáčknutí tlačítka "!".
 */
void Calc::on_Fact_clicked()
{
    double x = ui->Operand1->text().toDouble();
    double fact=1;
    for(int i=1; i<=x; i++)
    {
        fact = fact * i;
    }
    ui->Operand1->setText((QString)"0");
    ui->Operand2->setText(QString::number(fact));
    comma = false;
}

/**
 * Metoda on_sin_clicked.
 * 
 * @brief Zavolá se při zmáčknutí tlačítka "sin(x)".
 */
void Calc::on_sin_clicked()
{
    double x = ui->Operand1->text().toDouble();
    ui->Operand1->setText((QString)"0");
    ui->Operand2->setText(QString::number(sin(x)));
    comma = false;
}

/**
 * Metoda on_exp_clicked
 * 
 * @brief Zavolá se při zmáčknutí tlačítka "2^y".
 */
void Calc::on_exp_clicked()
{
    double x = ui->Operand1->text().toDouble();
    ui->Operand1->setText((QString)"0");
    ui->Operand2->setText(QString::number(exp2(x)));
    comma = false;
}

/*** Konec souboru calc.cpp ***/
