#ifndef CALC_H
#define CALC_H

#include <math.h>
#include <QKeyEvent>

extern int comma;

#define cislo \
if(ui->Operand2->text().toDouble() == 0) \
{ \
    comma = false; \
    ui->Operand2->setText(QString::number(ui->Operand1->text().toDouble())); \
    ui->Operand1->setText(QString::number(0)); \
}

#define pripocitaj(str) \
if(ui->Operand1->text() == "0") \
    ui->Operand1->setText((QString)str); \
else \
ui->Operand1->setText((QString)(ui->Operand1->text()+str));

#include <QMainWindow>


/**
 * Třída Calc
 * Obsahuje všechny metody co umí kalkulačka
 */ 
namespace Ui {
class Calc;
}

class Calc : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Calc(QWidget *parent = 0);
    ~Calc();
    
private slots:
    void on_deleno_clicked();

    void on_vypocet_clicked();

    void on_N1_clicked();

    void on_N2_clicked();

    void on_N3_clicked();

    void on_plus_clicked();

    void on_minus_clicked();

    void on_clear_clicked();

    void on_krat_clicked();

    void on_N4_clicked();

    void on_N5_clicked();

    void on_N6_clicked();

    void on_N7_clicked();

    void on_N8_clicked();

    void on_N9_clicked();

    void on_N0_clicked();

    void on_comma_clicked();

    void on_Fact_clicked();

    void on_sin_clicked();

    void on_exp_clicked();

private:
    Ui::Calc *ui;
    void keyPressEvent(QKeyEvent *event);
};

#endif // CALC_H
