/****************************************************************************
** Meta object code from reading C++ file 'calc.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.0.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../IVS3-1/calc.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'calc.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.0.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Calc_t {
    QByteArrayData data[22];
    char stringdata[313];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_Calc_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_Calc_t qt_meta_stringdata_Calc = {
    {
QT_MOC_LITERAL(0, 0, 4),
QT_MOC_LITERAL(1, 5, 17),
QT_MOC_LITERAL(2, 23, 0),
QT_MOC_LITERAL(3, 24, 18),
QT_MOC_LITERAL(4, 43, 13),
QT_MOC_LITERAL(5, 57, 13),
QT_MOC_LITERAL(6, 71, 13),
QT_MOC_LITERAL(7, 85, 15),
QT_MOC_LITERAL(8, 101, 16),
QT_MOC_LITERAL(9, 118, 16),
QT_MOC_LITERAL(10, 135, 15),
QT_MOC_LITERAL(11, 151, 13),
QT_MOC_LITERAL(12, 165, 13),
QT_MOC_LITERAL(13, 179, 13),
QT_MOC_LITERAL(14, 193, 13),
QT_MOC_LITERAL(15, 207, 13),
QT_MOC_LITERAL(16, 221, 13),
QT_MOC_LITERAL(17, 235, 13),
QT_MOC_LITERAL(18, 249, 16),
QT_MOC_LITERAL(19, 266, 15),
QT_MOC_LITERAL(20, 282, 14),
QT_MOC_LITERAL(21, 297, 14)
    },
    "Calc\0on_deleno_clicked\0\0on_vypocet_clicked\0"
    "on_N1_clicked\0on_N2_clicked\0on_N3_clicked\0"
    "on_plus_clicked\0on_minus_clicked\0"
    "on_clear_clicked\0on_krat_clicked\0"
    "on_N4_clicked\0on_N5_clicked\0on_N6_clicked\0"
    "on_N7_clicked\0on_N8_clicked\0on_N9_clicked\0"
    "on_N0_clicked\0on_comma_clicked\0"
    "on_Fact_clicked\0on_sin_clicked\0"
    "on_exp_clicked\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Calc[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  114,    2, 0x08,
       3,    0,  115,    2, 0x08,
       4,    0,  116,    2, 0x08,
       5,    0,  117,    2, 0x08,
       6,    0,  118,    2, 0x08,
       7,    0,  119,    2, 0x08,
       8,    0,  120,    2, 0x08,
       9,    0,  121,    2, 0x08,
      10,    0,  122,    2, 0x08,
      11,    0,  123,    2, 0x08,
      12,    0,  124,    2, 0x08,
      13,    0,  125,    2, 0x08,
      14,    0,  126,    2, 0x08,
      15,    0,  127,    2, 0x08,
      16,    0,  128,    2, 0x08,
      17,    0,  129,    2, 0x08,
      18,    0,  130,    2, 0x08,
      19,    0,  131,    2, 0x08,
      20,    0,  132,    2, 0x08,
      21,    0,  133,    2, 0x08,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Calc::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Calc *_t = static_cast<Calc *>(_o);
        switch (_id) {
        case 0: _t->on_deleno_clicked(); break;
        case 1: _t->on_vypocet_clicked(); break;
        case 2: _t->on_N1_clicked(); break;
        case 3: _t->on_N2_clicked(); break;
        case 4: _t->on_N3_clicked(); break;
        case 5: _t->on_plus_clicked(); break;
        case 6: _t->on_minus_clicked(); break;
        case 7: _t->on_clear_clicked(); break;
        case 8: _t->on_krat_clicked(); break;
        case 9: _t->on_N4_clicked(); break;
        case 10: _t->on_N5_clicked(); break;
        case 11: _t->on_N6_clicked(); break;
        case 12: _t->on_N7_clicked(); break;
        case 13: _t->on_N8_clicked(); break;
        case 14: _t->on_N9_clicked(); break;
        case 15: _t->on_N0_clicked(); break;
        case 16: _t->on_comma_clicked(); break;
        case 17: _t->on_Fact_clicked(); break;
        case 18: _t->on_sin_clicked(); break;
        case 19: _t->on_exp_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject Calc::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Calc.data,
      qt_meta_data_Calc,  qt_static_metacall, 0, 0}
};


const QMetaObject *Calc::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Calc::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Calc.stringdata))
        return static_cast<void*>(const_cast< Calc*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Calc::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 20;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
