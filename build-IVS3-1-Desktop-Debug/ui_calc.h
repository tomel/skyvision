/********************************************************************************
** Form generated from reading UI file 'calc.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALC_H
#define UI_CALC_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Calc
{
public:
    QWidget *centralWidget;
    QPushButton *vypocet;
    QLabel *Operand2;
    QLabel *Operand1;
    QFrame *line;
    QLabel *znak;
    QPushButton *plus;
    QPushButton *krat;
    QPushButton *minus;
    QPushButton *deleno;
    QPushButton *N1;
    QPushButton *N2;
    QPushButton *N3;
    QPushButton *clear;
    QPushButton *N4;
    QPushButton *N5;
    QPushButton *N6;
    QPushButton *N7;
    QPushButton *N8;
    QPushButton *N9;
    QPushButton *N0;
    QPushButton *sin;
    QPushButton *Fact;
    QPushButton *exp;
    QPushButton *comma;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Calc)
    {
        if (Calc->objectName().isEmpty())
            Calc->setObjectName(QStringLiteral("Calc"));
        Calc->resize(333, 359);
        centralWidget = new QWidget(Calc);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        vypocet = new QPushButton(centralWidget);
        vypocet->setObjectName(QStringLiteral("vypocet"));
        vypocet->setGeometry(QRect(195, 256, 128, 34));
        Operand2 = new QLabel(centralWidget);
        Operand2->setObjectName(QStringLiteral("Operand2"));
        Operand2->setGeometry(QRect(50, 10, 250, 40));
        Operand2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        Operand1 = new QLabel(centralWidget);
        Operand1->setObjectName(QStringLiteral("Operand1"));
        Operand1->setGeometry(QRect(50, 50, 250, 40));
        Operand1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(10, 90, 313, 3));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        znak = new QLabel(centralWidget);
        znak->setObjectName(QStringLiteral("znak"));
        znak->setEnabled(false);
        znak->setGeometry(QRect(10, 50, 40, 40));
        znak->setAlignment(Qt::AlignCenter);
        plus = new QPushButton(centralWidget);
        plus->setObjectName(QStringLiteral("plus"));
        plus->setGeometry(QRect(263, 100, 60, 31));
        krat = new QPushButton(centralWidget);
        krat->setObjectName(QStringLiteral("krat"));
        krat->setGeometry(QRect(263, 178, 60, 31));
        minus = new QPushButton(centralWidget);
        minus->setObjectName(QStringLiteral("minus"));
        minus->setGeometry(QRect(263, 139, 60, 31));
        deleno = new QPushButton(centralWidget);
        deleno->setObjectName(QStringLiteral("deleno"));
        deleno->setGeometry(QRect(263, 217, 60, 31));
        N1 = new QPushButton(centralWidget);
        N1->setObjectName(QStringLiteral("N1"));
        N1->setGeometry(QRect(10, 100, 50, 40));
        N2 = new QPushButton(centralWidget);
        N2->setObjectName(QStringLiteral("N2"));
        N2->setGeometry(QRect(70, 100, 50, 40));
        N3 = new QPushButton(centralWidget);
        N3->setObjectName(QStringLiteral("N3"));
        N3->setGeometry(QRect(130, 100, 50, 40));
        clear = new QPushButton(centralWidget);
        clear->setObjectName(QStringLiteral("clear"));
        clear->setGeometry(QRect(195, 100, 60, 31));
        N4 = new QPushButton(centralWidget);
        N4->setObjectName(QStringLiteral("N4"));
        N4->setGeometry(QRect(10, 150, 50, 40));
        N5 = new QPushButton(centralWidget);
        N5->setObjectName(QStringLiteral("N5"));
        N5->setGeometry(QRect(70, 150, 50, 40));
        N6 = new QPushButton(centralWidget);
        N6->setObjectName(QStringLiteral("N6"));
        N6->setGeometry(QRect(130, 150, 50, 40));
        N7 = new QPushButton(centralWidget);
        N7->setObjectName(QStringLiteral("N7"));
        N7->setGeometry(QRect(10, 200, 50, 40));
        N8 = new QPushButton(centralWidget);
        N8->setObjectName(QStringLiteral("N8"));
        N8->setGeometry(QRect(70, 200, 50, 40));
        N9 = new QPushButton(centralWidget);
        N9->setObjectName(QStringLiteral("N9"));
        N9->setGeometry(QRect(130, 200, 50, 40));
        N0 = new QPushButton(centralWidget);
        N0->setObjectName(QStringLiteral("N0"));
        N0->setGeometry(QRect(10, 250, 110, 40));
        sin = new QPushButton(centralWidget);
        sin->setObjectName(QStringLiteral("sin"));
        sin->setGeometry(QRect(195, 178, 60, 31));
        Fact = new QPushButton(centralWidget);
        Fact->setObjectName(QStringLiteral("Fact"));
        Fact->setGeometry(QRect(195, 217, 60, 31));
        exp = new QPushButton(centralWidget);
        exp->setObjectName(QStringLiteral("exp"));
        exp->setGeometry(QRect(195, 139, 60, 31));
        comma = new QPushButton(centralWidget);
        comma->setObjectName(QStringLiteral("comma"));
        comma->setGeometry(QRect(130, 250, 50, 40));
        Calc->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Calc);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 333, 21));
        Calc->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Calc);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        Calc->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Calc);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        Calc->setStatusBar(statusBar);

        retranslateUi(Calc);

        QMetaObject::connectSlotsByName(Calc);
    } // setupUi

    void retranslateUi(QMainWindow *Calc)
    {
        Calc->setWindowTitle(QApplication::translate("Calc", "Calc", 0));
        vypocet->setText(QApplication::translate("Calc", "=", 0));
        Operand2->setText(QApplication::translate("Calc", "Operand 2", 0));
        Operand1->setText(QApplication::translate("Calc", "Operand 1", 0));
        znak->setText(QApplication::translate("Calc", "+", 0));
        plus->setText(QApplication::translate("Calc", "+", 0));
        krat->setText(QApplication::translate("Calc", "\303\227", 0));
        minus->setText(QApplication::translate("Calc", "-", 0));
        deleno->setText(QApplication::translate("Calc", "\303\267", 0));
        N1->setText(QApplication::translate("Calc", "1", 0));
        N2->setText(QApplication::translate("Calc", "2", 0));
        N3->setText(QApplication::translate("Calc", "3", 0));
        clear->setText(QApplication::translate("Calc", "CLR", 0));
        N4->setText(QApplication::translate("Calc", "4", 0));
        N5->setText(QApplication::translate("Calc", "5", 0));
        N6->setText(QApplication::translate("Calc", "6", 0));
        N7->setText(QApplication::translate("Calc", "7", 0));
        N8->setText(QApplication::translate("Calc", "8", 0));
        N9->setText(QApplication::translate("Calc", "9", 0));
        N0->setText(QApplication::translate("Calc", "0", 0));
        sin->setText(QApplication::translate("Calc", "sin(x)", 0));
        Fact->setText(QApplication::translate("Calc", "x!", 0));
        exp->setText(QApplication::translate("Calc", "2^y", 0));
        comma->setText(QApplication::translate("Calc", ",", 0));
    } // retranslateUi

};

namespace Ui {
    class Calc: public Ui_Calc {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALC_H
